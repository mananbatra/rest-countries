import React, { useContext } from 'react'
import BorderCountries from './BorderCountries'
import { Link } from 'react-router-dom'
import './CountryDetails.css'
import { DarkThemeContext } from '../../DarkThemeContext'

const CountryDetails = ({displayCountry, countries}) => {

    const {darkTheme}=useContext(DarkThemeContext)
    const borderCountry=displayCountry.borders;
    const currency=displayCountry.currencies
    const language=displayCountry.languages

  return (
    <div className={`card-container ${darkTheme?"dark-theme":''}`}>
        <div ><Link to='/' className="country-link"><button className='backButton'><i className="fa-solid fa-arrow-left" style={{ color: darkTheme ? '#ffffff' : '#000000' }}></i>&nbsp;&nbsp;Back</button></Link></div>
        <div className='card-design'>
            <div className='country-flag'>
                <img src={displayCountry.flags.svg}></img>
            </div>
            <div className='country-detail'>
                <div><h3>{displayCountry.name.common}</h3></div>
                <div className='important-detail'>
                <div className='detail-col'>
                    <div className='detail'><b>Native Name</b>:{Object.values(displayCountry.name.nativeName)[0].common}</div>
                    <div className='detail'><b>Population</b>: {displayCountry.population}</div>
                    <div className='detail'><b>Region</b>: {displayCountry.region}</div>
                    <div className='detail'><b>SubRegion</b>: {displayCountry.subregion}</div>
                    <div className='detail'><b>Capital</b>: {displayCountry.capital}</div>
                </div>
                <div className='detail-col'>
                    <div className='detail'><b>Top Level Domain </b>: {displayCountry.tld.join(",")}</div>
                    <div className='detail'><b>Currencies</b>: {currency?Object.values(displayCountry.currencies)[0].name:null}</div>
                    <div className='detail'><b>Languages</b>: {language?Object.values(displayCountry.languages).join(","):null}</div>
                </div>

                </div>
                {borderCountry==undefined?null:<div className='neighbors'><b id='b1'>Border Countries</b>:&nbsp;&nbsp; <BorderCountries borderCountry={displayCountry.borders} countries={countries}/></div>}
            </div>
        </div>
    </div>
  )
}

export default CountryDetails