import React from 'react'
import TitleBar from '../header-section/TitleBar'
import CountryDetails from './CountryDetails'
import { useParams } from 'react-router-dom'
import Loader from '../loader/Loader'

const CountryPage = ({countries}) => {

  const{id}=useParams()

  if(countries.length==0)
  {
    return <Loader/>
  }

  const displayCountry= countries.find((country)=>
  {
    return country.ccn3===id
  })


  return (
    <>
        <TitleBar/>
        {displayCountry ? <CountryDetails displayCountry={displayCountry} countries={countries}/> : <h3> Not A valid Country Url</h3>}
    </>
  )
}

export default CountryPage