import React from 'react'
import { Link } from 'react-router-dom'

const BorderCountries = ({countries, borderCountry}) => {

    const filteredCountries = countries.filter((country) =>
            borderCountry.includes(country.cca3)
            );


  return (
    <div className='neighbour-buttons'>
        {filteredCountries.map((country)=>
        {
            return(
                
                <Link  key={country.ccn3} className="country-link" to={`/countries/${country.ccn3}` }>
                    <button>{country.name.common}</button>
                </Link> 
                             
            )
        })}
        </div> 
  )
}

export default BorderCountries