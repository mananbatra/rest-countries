import React from 'react'

const SortBy = ({ setSortBy, setSortAscending }) => {
  const handleSort = (event) => {
    const selectedValue = event.target.value;
    const [sortAsc, sortBy] = selectedValue.split(':');
    setSortBy(sortBy);
    setSortAscending(sortAsc === 'true');
  };

  return (
    <div className="sort-tab">
      <select onChange={handleSort} id="dropdown">
        <option value="">Sort by</option>
        <option value="true:population">Ascending-(By Population)</option>
        <option value="false:population">Descending-(By Population)</option>
        <option value="true:area">Ascending-(By Area)</option>
        <option value="false:area">Descending-(By Area)</option>
      </select>
    </div>
  );
};

export default SortBy