import React from 'react'

const SubRegion = ({setSubRegion,subregionArr}) => {


    const handleRegionChange = (event) => {
        setSubRegion(event.target.value);
      };

      // console.log(subregionArr);
  return (

    <div className="sub-region">
      <select onChange={handleRegionChange}
        id="dropdown"
      >
        <option value="">Filter by SubRegion</option>
        {[...subregionArr].map((subregion)=>
        {
            return <option key={subregion} value={subregion}>{subregion}</option>
        })}
      </select>
    </div>
  )
}

export default SubRegion