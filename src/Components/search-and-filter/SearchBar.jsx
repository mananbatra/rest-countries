import React, { useContext } from "react";
import "../../App.css";
import { DarkThemeContext } from "../../DarkThemeContext";

const SearchBar = ({ searchQuery, setSearchQuery}) => {

  const {darkTheme}=useContext(DarkThemeContext)
    const handleSearchChange = (event) => {
      setSearchQuery(event.target.value);
    };
  
    return (
      <div className={`search-bar ${darkTheme?"dark-theme":''}`}>
        <i className="fa-solid fa-magnifying-glass" style={{color: `${darkTheme ? "#ffffff" : "#000000"}`}}></i>
        <input
          type="text"
          className="search-input"
          placeholder="Search for a country..."
          value={searchQuery}
          onChange={handleSearchChange}
        />
      </div>
    );
  };

export default SearchBar;
