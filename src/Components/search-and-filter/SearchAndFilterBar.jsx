import React,{useContext, useState} from 'react'
import SearchBar from './SearchBar'
import FilterBar from './FilterBar'
import '../../App.css'
import SortBy from './SortBy';
import SubRegion from './SubRegion';
import { DarkThemeContext } from '../../DarkThemeContext';

const SearchAndFilterBar = ({ searchQuery, setSearchQuery ,setRegion , setSortBy, setSubRegion, subregionArr, setSortAscending}) => {

  const {darkTheme}=useContext(DarkThemeContext)
    return (
      <div className={`nav-bar ${darkTheme?"dark-theme":''}`}>
        <SearchBar searchQuery={searchQuery} setSearchQuery={setSearchQuery} />
        <SortBy setSortBy={setSortBy} setSortAscending={setSortAscending}/>
        <FilterBar setRegion={setRegion} setSubRegion={setSubRegion}/>
        <SubRegion setSubRegion={setSubRegion} subregionArr={subregionArr}/>
      </div>
    );
  };

export default SearchAndFilterBar