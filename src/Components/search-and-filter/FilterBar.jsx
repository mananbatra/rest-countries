import React from "react";

const FilterBar = ({setRegion,setSubRegion}) => {

    const handleRegionChange = (event) => {
        setRegion(event.target.value);
          setSubRegion("");
      };
  return (
    <div className="filter-tab">
      <select onChange={handleRegionChange}
        id="dropdown"
      >
        <option value="">Filter by Region</option>
        <option value="Oceania">Oceania</option>
        <option value="Americas">Americas</option>
        <option value="Asia">Asia</option>
        <option value="Europe">Europe</option>
        <option value="Africa">Africa</option>
        <option value="Antarctic">Antarctic</option>
      </select>
    </div>
  );
};

export default FilterBar;
