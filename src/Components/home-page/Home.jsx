import React from "react";
import { useState } from "react";
import "../../App.css";
import ContainerOfCards from "../container-country-cards/ContainerOfCards";
import TitleBar from "../header-section/TitleBar";
import SearchAndFilterBar from "../search-and-filter/SearchAndFilterBar";
import "bootstrap/dist/css/bootstrap.min.css";
import Loader from "../loader/Loader";

function filterCountriesByRegion(countries, region) {
  return countries.filter((country) => {
    return country.region.toLowerCase().includes(region.toLowerCase());
  });
}

function getSubregionsForRegion(regionCountries) {
  let listOfSubregions = regionCountries.map((country) => {
    return country.region === "Antarctic" ? "No SubRegion" : country.subregion;
  });
  return new Set(listOfSubregions);
}

function filterCountriesBySubregion(regionCountries, subregion) {
  return regionCountries.filter((country) => {
    if (country.subregion === undefined) {
      return country;
    }
    return country.subregion?.toLowerCase().includes(subregion.toLowerCase());
  });
}

function searchCountriesByName(countries, searchQuery) {
  return countries.filter((country) =>
    country.name.common.toLowerCase().includes(searchQuery.toLowerCase().trim())
  );
}

function sortCountries(sortedData, sortBy, sortAscending) {
  console.log(sortAscending, sortBy);
  if (sortAscending) {
    sortedData.sort(
      (countryA, countryB) => countryA[sortBy] - countryB[sortBy]
    );
  } else {
    sortedData.sort(
      (countryA, countryB) => countryB[sortBy] - countryA[sortBy]
    );
  }
}

const Home = ({ countries, isLoading }) => {
  const [searchQuery, setSearchQuery] = useState("");
  const [region, setRegion] = useState("");
  const [sortBy, setSortBy] = useState("");
  const [subregion, setSubRegion] = useState("");
  const [sortAscending, setSortAscending] = useState(true);

  const regionCountries = filterCountriesByRegion(countries, region); // function for region

  let setOfSubregion = new Set();
  if (region !== "") {
    setOfSubregion = getSubregionsForRegion(regionCountries);
  }

  const subRegionCountries = filterCountriesBySubregion(
    regionCountries,
    subregion
  ); // function for subRegion

  const searchedCountries = searchCountriesByName(
    subRegionCountries,
    searchQuery
  ); // function for search results

  if (sortBy !== "") {
    sortCountries(searchedCountries, sortBy, sortAscending); // sort by area / population
  }

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <TitleBar />
          <SearchAndFilterBar
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
            setRegion={setRegion}
            setSortBy={setSortBy}
            setSortAscending={setSortAscending}
            setSubRegion={setSubRegion}
            subregionArr={setOfSubregion}
          />
          <ContainerOfCards countries={searchedCountries} />
        </>
      )}
    </>
  );
};

export default Home;
