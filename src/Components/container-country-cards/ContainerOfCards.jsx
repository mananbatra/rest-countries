import React, { useContext } from "react";
import Card from "./Card";
import "../../App.css";
import { DarkThemeContext } from "../../DarkThemeContext";

const ContainerOfCards = ({ countries}) => {

  const {darkTheme}=useContext(DarkThemeContext)
  // console.log(countries);
  return (
    <div className={`countryCards ${darkTheme ? "dark-theme" : ""}`}>
      {countries.length === 0 ? (
        <div className="no-countries">Sorry No Countries To Display</div>
      ) : (
        countries.map((country, index) => {
          // console.log(country,"jhc<wfjdcsh")
          return (
            <Card 
              key={country.cca3}
              imgSource={country.flags.png}
              country={country.name.common}
              population={country.population}
              capital={country.capital}
              region={country.region}
              ccn3={country.ccn3}
            />
           
          );
        })
      )}
    </div>
  );
};

export default ContainerOfCards;
