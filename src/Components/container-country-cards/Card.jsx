import React, { useContext } from 'react'
import '../../App.css'
import { DarkThemeContext } from '../../DarkThemeContext'
import { Link } from 'react-router-dom'

const Card = ({imgSource,country,population,region,capital,ccn3}) => {

  const {darkTheme}=useContext(DarkThemeContext)

  return (
   
    <div className={`country-card ${darkTheme?"dark-theme":''}`}>
      <Link className="country-link" to={`countries/${ccn3}`}>
        <div className='flag'><img src={imgSource}></img></div>
        <div className='country-details'>
            <div className='countryName'>{country}</div>
            <div className='countryDetail'><b>Population</b>: {population}</div>
            <div className='countryDetail'><b>Region</b>: {region}</div>
            <div className='countryDetail'><b>Capital</b>: {capital}</div>
        </div>
      </Link>
    </div>
  )
}

export default Card;