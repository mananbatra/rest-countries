import React from 'react'
import './ErrorPage.css'

const ErrorPage = () => {
  return (
    <div className="error-container">
    <div className="error-content">
      <h1>Oops! Something went wrong.</h1>
      <p>We apologize for the inconvenience. The page you are looking for might be temporarily unavailable or does not exist.</p>
      <a href="/" className="button">Go back to homepage</a>
    </div>
  </div>
);
}

export default ErrorPage