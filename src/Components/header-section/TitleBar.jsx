import React, { useContext } from "react";
import ThemeSwitch from "./ThemeSwitch";
import '../../App.css'
import { DarkThemeContext } from "../../DarkThemeContext";
const TitleBar = () => {

  const {darkTheme}=useContext(DarkThemeContext)
    return (
      <div className={`title ${darkTheme ? "dark-theme" : ""}`}>
        <h3>Where in the World?</h3>
        <ThemeSwitch />
      </div>
    );
  };

export default TitleBar;
