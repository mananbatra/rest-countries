import React, { useContext } from 'react';
import { DarkThemeContext } from '../../DarkThemeContext';

const ThemeSwitch = () => {
  const { darkTheme, setDarkTheme } = useContext(DarkThemeContext);

  const toggleTheme = () => {
    setDarkTheme(prevDarkTheme => !prevDarkTheme);
  };

  return (
    <button className='themeChange' onClick={toggleTheme}>
      <i
        className={`fa-regular ${darkTheme ? 'fa-sun' : 'fa-moon'}`}
        style={{ color: darkTheme ? '#ffff00' : '#000000' }}
      ></i>
      {darkTheme ? 'Light Mode' : 'Dark Mode'}
    </button>
  );
};

export default ThemeSwitch;