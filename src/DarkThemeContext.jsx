import React, { createContext, useState } from 'react';

const DarkThemeContext = createContext();

const DarkThemeContextProvider = ({ children }) => {
  const [darkTheme, setDarkTheme] = useState(false);

  return (
    <DarkThemeContext.Provider value={{ darkTheme, setDarkTheme }}>
      {children}
    </DarkThemeContext.Provider>
  );
};

export { DarkThemeContext, DarkThemeContextProvider };
