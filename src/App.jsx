import Home from "./Components/home-page/Home";
import CountryPage from "./Components/country-page/CountryPage";
import { useEffect, useState } from "react";
import ErrorPage from "./Components/error-page/ErrorPage";
import { Route, Routes } from "react-router-dom";

function App() {
  const [countries, SetCountries] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        SetCountries(data);
        // console.log(data);
      })
      .catch((err) => {
        console.log(err, "my error");
        setError(err);
      })
      .finally(() => {
        setIsLoading(false); // Set isLoading to false in any case (success or error)
      });
  }, []);

  if(error!==null){
    return(
    <ErrorPage/>
    );
  }


  return (
    <>
    <Routes>
      <Route path="/" element={<Home countries={countries} isLoading={isLoading}/>}/>
      <Route path="countries/:id" element={<CountryPage countries={countries}/>}/>
      <Route path="*" element={<ErrorPage/>}/>
    </Routes>
    </>
  );
}

export default App;
